# Brainfuck compiler written in Python, going to a .py file1

import sys

chars = ["+","-","[","]","<",">",".",","]

file1 = open(sys.argv[1],"r")
file2 = open(sys.argv[3],"w")

while True:
    char = file1.read(1)
    if char in chars:
        if char == "+": file2.write("Plus")
        elif char == "-": file2.write("Minus")
        elif char == ".": file2.write("Dot")
        elif char == "<": file2.write("Larrow")
        elif char == ">": file2.write("Rarrow")
        elif char == "[": file2.write("Lbracket")
        elif char == "]": file2.write("Rbracket")
        else: file2.write("COMMA")
    else:
        pass
    if len(char) == 0:
        break

### something where we keep track of variables as elements of a tuple ### (key, value) so that we can continue to reference these variables as the assignments change

file1.close()
file2.close()
