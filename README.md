# Brainfuck Repo
This is a repository for my work with the 'esoteric' Brainfuck language.

## What's in the repo?
It contains some example files, as well as a Python script to minimize the files for optimum efficiency with a web-based interpreter.
The repo also contains a notes.txt file, with some of my own documentation for the language.
This documentation is in no way complete, but it has all of the essentials to get started.
The compiler is very much beta software. 
It currently takes an input file and returns a string version of the origianl brainfuck code.

## Python-Brainfuck Directory
This is a repo that I cloned from pocmo over on GitHub <a href="https://github.com/pocmo/Python-Brainfuck">here</a>. Go check out his repo too.
It's a command-line interpreter for brainfuck, written in Python.